package it.unisalento.pas.smartcitywastemanagement.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import it.unisalento.pas.smartcitywastemanagement.domain.Vehicle;
import it.unisalento.pas.smartcitywastemanagement.dto.VehicleDTO;
import it.unisalento.pas.smartcitywastemanagement.repositories.VehicleRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/vehicles")
public class VehicleRestController {

    @Autowired
    VehicleRepository vehicleRepository;


    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @PostMapping("/")
    public VehicleDTO createVehicle(@RequestBody VehicleDTO vehicleDTO) {
        Vehicle vehicle = new Vehicle();
        // Mapping properties from DTO to domain
        vehicle.setId(vehicleDTO.getId());
        vehicle.setVehicleType(vehicleDTO.getVehicleType());
        vehicle.setName(vehicleDTO.getName());
        vehicle.setCargoWeight(vehicleDTO.getCargoWeight());
        vehicle.setServiceStatus(vehicleDTO.getServiceStatus());

        vehicle = vehicleRepository.save(vehicle);
        return vehicleDTO;
    }


    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @GetMapping("/")
    public List<VehicleDTO> getAllVehicles() {
        List<VehicleDTO> dtoList = new ArrayList<>();
        List<Vehicle> vehicles = vehicleRepository.findAll();
        for (Vehicle vehicle : vehicles) {
            VehicleDTO dto = new VehicleDTO();
            // Mapping properties from domain to DTO
            dto.setId(vehicle.getId());
            dto.setVehicleType(vehicle.getVehicleType());
            dto.setName(vehicle.getName());
            dto.setCargoWeight(vehicle.getCargoWeight());
            dto.setServiceStatus(vehicle.getServiceStatus());

            dtoList.add(dto);
        }
        return dtoList;
    }


    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','EMPLOYEE')")
    @GetMapping("/{id}")
    public ResponseEntity<VehicleDTO> getVehicleById(@PathVariable String id) {
        return vehicleRepository.findById(id).map(vehicle -> {
            VehicleDTO dto = new VehicleDTO();
            // Mapping properties from domain to DTO
            dto.setId(vehicle.getId());
            dto.setVehicleType(vehicle.getVehicleType());
            dto.setName(vehicle.getName());
            dto.setCargoWeight(vehicle.getCargoWeight());
            dto.setServiceStatus(vehicle.getServiceStatus());
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<VehicleDTO> updateVehicle(@PathVariable String id, @RequestBody VehicleDTO vehicleDTO) {
        return vehicleRepository.findById(id).map(existingVehicle -> {
            // Mapping updated properties from DTO to existing domain object
            existingVehicle.setVehicleType(vehicleDTO.getVehicleType());
            existingVehicle.setName(vehicleDTO.getName());
            existingVehicle.setCargoWeight(vehicleDTO.getCargoWeight());
            existingVehicle.setServiceStatus(vehicleDTO.getServiceStatus());
            vehicleRepository.save(existingVehicle);

            return new ResponseEntity<>(vehicleDTO, HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteVehicleById(@PathVariable String id) {
        Optional<Vehicle> vehicle = vehicleRepository.findById(id);

        if (vehicle.isPresent()) {
            vehicleRepository.deleteById(id);
            return ResponseEntity.ok().build(); // Restituisce solo il codice di stato HTTP 200
        } else {
            return ResponseEntity.notFound().build(); // Restituisce il codice di stato HTTP 404
        }
    }



    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @GetMapping("/status/offRoute")
    public ResponseEntity<List<VehicleDTO>> getVehiclesOffRoute() {
        List<VehicleDTO> dtoList = new ArrayList<>();
        List<Vehicle> vehicles = vehicleRepository.findByServiceStatus("offRoute");
        for (Vehicle vehicle : vehicles) {
            VehicleDTO dto = new VehicleDTO();
            // Mapping properties from domain to DTO
            dto.setId(vehicle.getId());
            dto.setVehicleType(vehicle.getVehicleType());
            dto.setName(vehicle.getName());
            dto.setCargoWeight(vehicle.getCargoWeight());
            dto.setServiceStatus(vehicle.getServiceStatus());

            dtoList.add(dto);
        }
        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','EMPLOYEE')")
    @PutMapping("/{id}/updateStatus")
    public ResponseEntity<VehicleDTO> updateVehicleStatus(@PathVariable String id, @RequestBody String status) {
        return vehicleRepository.findById(id).map(vehicle -> {
            // Aggiorna lo stato del veicolo
            vehicle.setServiceStatus(status);
            vehicle = vehicleRepository.save(vehicle);

            // Crea e restituisce il DTO aggiornato
            VehicleDTO dto = new VehicleDTO();
            dto.setId(vehicle.getId());
            dto.setVehicleType(vehicle.getVehicleType());
            dto.setName(vehicle.getName());
            dto.setCargoWeight(vehicle.getCargoWeight());
            dto.setServiceStatus(vehicle.getServiceStatus());
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



}
