package it.unisalento.pas.smartcitywastemanagement.restcontrollers;

import it.unisalento.pas.smartcitywastemanagement.domain.Vehicle;
import it.unisalento.pas.smartcitywastemanagement.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import it.unisalento.pas.smartcitywastemanagement.domain.Path;
import it.unisalento.pas.smartcitywastemanagement.dto.PathDTO;
import it.unisalento.pas.smartcitywastemanagement.repositories.PathRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/paths")
public class PathRestController {

    @Autowired
    PathRepository pathRepository;

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")

    @GetMapping("/")
    public List<PathDTO> getAllPaths() {
        List<PathDTO> dtoList = new ArrayList<>();
        List<Path> paths = pathRepository.findAll();
        for (Path path : paths) {
            PathDTO dto = new PathDTO();

            dto.setId(path.getId());
            dto.setRoute(path.getHasRoute());
            dto.setDay(path.getHasDay());
            dto.setStatoPercorso(path.getStatoPercorso());

            dto.setVehicle(path.getHasVehicle());
            dto.setDriver(path.getHasDriver());

            dtoList.add(dto);
        }
        return dtoList;
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")

    @GetMapping("/{id}")
    public PathDTO getPathById(@PathVariable String id) {
        Path path = pathRepository.findById(id).orElse(null);
        if (path == null) {
            return null;
        }
        PathDTO dto = new PathDTO();

        dto.setId(path.getId());
        dto.setRoute(path.getHasRoute());
        dto.setDay(path.getHasDay());
        dto.setStatoPercorso(path.getStatoPercorso());
        dto.setVehicle(path.getHasVehicle());
        dto.setDriver(path.getHasDriver());
        return dto;
    }

    @Autowired
    VehicleRepository vehicleRepository;

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @PostMapping("/")
    public PathDTO createPath(@RequestBody PathDTO pathDTO) {
        Path path = new Path();

        path.setId(pathDTO.getId());
        path.setHasRoute(pathDTO.getRoute());
        path.setHasDay(pathDTO.getDay());
        path.setHasVehicle(pathDTO.getVehicle());
        path.setHasDriver(pathDTO.getDriver());
        path.setStatoPercorso(pathDTO.getStatoPercorso());


        path = pathRepository.save(path);


        if (path.getHasVehicle() != null && !path.getHasVehicle().isEmpty()) {
            Vehicle vehicle = vehicleRepository.findById(path.getHasVehicle()).orElse(null);
            if (vehicle != null) {
                vehicle.setServiceStatus("onRoute");
                vehicleRepository.save(vehicle);
            }
        }

        pathDTO.setId(path.getId());
        return pathDTO;
    }



    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePathById(@PathVariable String id) {
        Optional<Path> pathOptional = pathRepository.findById(id);
        if (!pathOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Path path = pathOptional.get();
        String vehicleId = path.getHasVehicle();


        if (vehicleId != null && !vehicleId.isEmpty()) {
            Optional<Vehicle> vehicleOptional = vehicleRepository.findById(vehicleId);
            if (vehicleOptional.isPresent()) {
                Vehicle vehicle = vehicleOptional.get();
                vehicle.setServiceStatus("offRoute");
                vehicleRepository.save(vehicle);
            }
        }


        pathRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN', 'EMPLOYEE')")
    @GetMapping("/driver/{driverId}")
    public List<PathDTO> getPathsByDriverId(@PathVariable String driverId) {
        List<Path> paths = pathRepository.findByHasDriver(driverId);
        List<PathDTO> dtoList = new ArrayList<>();
        for (Path path : paths) {
            PathDTO dto = new PathDTO();

            dto.setId(path.getId());
            dto.setRoute(path.getHasRoute());
            dto.setDay(path.getHasDay());
            dto.setVehicle(path.getHasVehicle());
            dto.setDriver(path.getHasDriver());
            dto.setStatoPercorso(path.getStatoPercorso());
            dtoList.add(dto);
        }
        return dtoList;
    }


    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','EMPLOYEE')")
    @PutMapping("/{id}/updateStatus")
    public ResponseEntity<PathDTO> updatePathStatus(@PathVariable String id, @RequestBody String stato) {
        Optional<Path> pathOptional = pathRepository.findById(id);

        if (!pathOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Path path = pathOptional.get();
        path.setStatoPercorso(stato);
        path = pathRepository.save(path);

        PathDTO dto = mapPathToDTO(path);
        return ResponseEntity.ok(dto);
    }

    private PathDTO mapPathToDTO(Path path) {
        PathDTO dto = new PathDTO();
        dto.setId(path.getId());
        dto.setRoute(path.getHasRoute());
        dto.setDay(path.getHasDay());
        dto.setVehicle(path.getHasVehicle());
        dto.setDriver(path.getHasDriver());
        dto.setStatoPercorso(path.getStatoPercorso());
        return dto;
    }




}
