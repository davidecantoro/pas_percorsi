package it.unisalento.pas.smartcitywastemanagement.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Vehicle")
public class Vehicle {

    @Id
    private String id;
    private String vehicleType;
    private String name;
    private int cargoWeight;
    private String serviceStatus;
    private String type;
    private String vehiclePlateIdentifier;
    private List<String> category; // Aggiunta della nuova proprietà category


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(int cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVehiclePlateIdentifier() {
        return vehiclePlateIdentifier;
    }

    public void setVehiclePlateIdentifier(String vehiclePlateIdentifier) {
        this.vehiclePlateIdentifier = vehiclePlateIdentifier;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }
}
