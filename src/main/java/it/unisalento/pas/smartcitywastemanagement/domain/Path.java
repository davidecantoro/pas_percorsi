package it.unisalento.pas.smartcitywastemanagement.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Path")
public class Path {

    @Id
    private String id;
    private String[] hasRoute;
    private Date hasDay;
    private String hasVehicle;
    private String hasDriver;
    private String statoPercorso;

    // Constructors, Getters and Setters

    public Path() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getHasRoute() {
        return hasRoute;
    }

    public void setHasRoute(String[] hasRoute) {
        this.hasRoute = hasRoute;
    }

    public Date getHasDay() {
        return hasDay;
    }

    public void setHasDay(Date hasDay) {
        this.hasDay = hasDay;
    }




    public String getHasVehicle() {
        return hasVehicle;
    }

    public void setHasVehicle(String hasVehicle) {
        this.hasVehicle = hasVehicle;
    }

    public String getHasDriver() {
        return hasDriver;
    }

    public void setHasDriver(String hasDriver) {
        this.hasDriver = hasDriver;
    }

    public String getStatoPercorso() {
        return statoPercorso;
    }

    public void setStatoPercorso(String statoPercorso) {
        this.statoPercorso = statoPercorso;
    }

}
