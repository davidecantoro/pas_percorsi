package it.unisalento.pas.smartcitywastemanagement.repositories;

import it.unisalento.pas.smartcitywastemanagement.domain.Path;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PathRepository extends MongoRepository<Path, String> {
    List<Path> findByHasDriver(String driverId);
    List<Path> findByStatoPercorso(String statoPercorso);

}

