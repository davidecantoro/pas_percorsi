package it.unisalento.pas.smartcitywastemanagement.dto;

import java.util.Date;

public class PathDTO {

    private String id;
    private String[] route;
    private Date day;

    private String vehicle;
    private String driver;
    private String statoPercorso;

    // Getters and Setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getRoute() {
        return route;
    }

    public void setRoute(String[] route) {
        this.route = route;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }



    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getStatoPercorso() {
        return statoPercorso;
    }

    public void setStatoPercorso(String statoPercorso) {
        this.statoPercorso = statoPercorso;
    }
}
